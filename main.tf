resource "aws_vpc" "vpc_in_pmr_module" {
  cidr_block = "${var.cidr}"

  tags = {
    Name = "vpc-from-pmr-module"
  }
}

variable "cidr" {
  description = "CIDR for use in VPC"
  default = "172.16.0.0/16"
}

output "vpc_id" {
  value = "${aws_vpc.vpc_in_pmr_module.id}"
}
